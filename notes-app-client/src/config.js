export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  s3: {
    BUCKET: "gassettj-notes-uploads"
  },
  apiGateway: {
    URL: "https://54dzkvg321.execute-api.us-east-1.amazonaws.com/prod",
    REGION: "us-east-1"
  },
  cognito: {
	REGION: "us-east-1",
	IDENTITY_POOL_ID: "us-east-1:4c98e1ac-cf8d-443d-bb32-04821cd35c13",
    USER_POOL_ID: "us-east-1_W1fz49SKn",
    APP_CLIENT_ID: "478gq9g6jr28gi9cmapc3ko28o"
  }
};